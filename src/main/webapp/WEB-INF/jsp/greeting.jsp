<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Greetings!</title>
</head>
<body>
    <p>Greetings from Veracode, ${greetingName}!</p>
</body>
</html>